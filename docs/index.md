# Welcome!

**Welcome to the Official Unofficial documentation for [Mindustry](https://github.com/Anuken/Mindustry)!** This is still very limited, and *very* under construction. This aims to fully cover the unstable and unfinished 4.0 builds, just as the old (and still unfinished, sadly) [3.5 Docs](https://mindustry.wikia.com/wiki/Mindustry_Wiki) also aimed to do. 

**Contribution is highly encouraged.** If you find anything in the game that is not listed in these articles, please feel free to hop onto the [Docs' GitLab repo](https://gitlab.com/MindustryModders/mindustry-documentation) and open a Merge Request with the changes. If you are new to Markdown, I strongly advise you to visit this [beautiful guide](https://www.markdownguide.org/) by Matt Cone. It is jam-packed with resources to help you learn Markdown.

**To get started with Mindustry, head on over to [here](GettingStarted.md)!**

**Important!** Most public values (in technical section) are protected as the data is taken from the MML repo.
